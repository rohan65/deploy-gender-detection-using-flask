import pandas as pd
import os, cv2, requests, io
import xlrd, numpy
import random
from sklearn.metrics import confusion_matrix
import numpy as np
from PIL import Image , ImageOps  
from numpy import asarray
from tensorflow.keras.layers import Input,Lambda,Dense,Flatten,GlobalMaxPool2D,Dropout,MaxPool2D,Conv2D
from tensorflow.keras.models import Model,Sequential
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
from numpy import asarray
from sklearn.model_selection import train_test_split


male_data=pd.read_csv(r'/home/ubuntu/male_data.csv')
female_data=pd.read_excel(r'/home/ubuntu/female_data.xlsx')

training_data=[]
img_size=224

for i in range(male_data.shape[0]):
    response = requests.get(male_data.image_url[i])
    image_byte = io.BytesIO(response.content)
    image = Image.open(image_byte)
    image=image.resize((224,224))
    image = numpy.array(image)
    training_data.append([image,1])
for i in range(female_data.shape[0]):
    response = requests.get(female_data.image_url[i])
    image_bytes = io.BytesIO(response.content)
    image = Image.open(image_bytes)  
    image=image.resize((224,224))
    image = numpy.array(image)
    training_data.append([image,0])
random.shuffle(training_data)
x=[]
y=[]
for image,gender in training_data:
    x.append(image)
    y.append(gender)
x=np.array(x)
y=np.array(y)
#x=x.reshape(-1,img_size,img_size,3)
x=x.reshape(-1,img_size,img_size,3)
x_train, x_, y_train, y_=train_test_split(x,y,test_size=0.2)
x_test, x_valid, y_test, y_valid=train_test_split(x_,  y_,test_size=0.5)
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

valid_datagen = ImageDataGenerator(rescale = 1./255)
training_set = train_datagen.flow(x_train,y_train,
                                                 
                                                 batch_size = 32,
                                                 )
validation_set = valid_datagen.flow(x_valid,y_valid,
                                            
                                            batch_size = 32,
                                            )


img_size=[224,224]
vgg = VGG16(input_shape=img_size + [3], weights='imagenet', include_top=False)
for layer in vgg.layers:
  layer.trainable = False
x = Flatten()(vgg.output)
prediction = Dense(2, activation='softmax')(x)
model = Model(inputs=vgg.input, outputs=prediction)
model.compile(
  loss='binary_crossentropy',
  optimizer='adam',
  metrics=['accuracy']
)



model.fit(
  training_set,
  validation_data=validation_set,
  epochs=8,
  steps_per_epoch=len(training_set),
  validation_steps=len(validation_set)
)
y_pred=model.predict(x_test)
y_pred_final=[]


for i in range(len(y_pred)):
    y_pred_final.append(y_pred[i][1])
y_pred_final=np.round(y_pred_final)
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(y_pred_final,y_test)
model_accuracy=(cm[0][0]+cm[1][1]) / ( cm[1][0] + cm[0][1]+cm[0][0]+cm[1][1])
print(model_accuracy )