import os, cv2
import random
from sklearn.metrics import confusion_matrix
import numpy as np
from PIL import Image , ImageOps  
from numpy import asarray
from tensorflow.keras.layers import Input,Lambda,Dense,Flatten,GlobalMaxPool2D,Dropout,MaxPool2D,Conv2D
from tensorflow.keras.models import Model,Sequential
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
#from tensorflow.keras.callbacks import EarlyStopping




train_datagen = ImageDataGenerator(rescale = 1./255,
                                    shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

valid_datagen = ImageDataGenerator(rescale = 1./255)



training_set = train_datagen.flow_from_directory(r'/home/ubuntu/tmface1/train',
                                                 target_size = (224, 224),
                                                 batch_size = 32,
                                                 class_mode = 'binary')

validation_set = valid_datagen.flow_from_directory(r'/home/ubuntu/tmface1/validation',
                                            target_size = (224, 224),
                                            batch_size = 32,
                                            class_mode = 'binary')



img_size=[224,224]
vgg = VGG16(input_shape=img_size + [3], weights='imagenet', include_top=False)
for layer in vgg.layers:
  layer.trainable = False
x = Flatten()(vgg.output)
prediction = Dense(1, activation='sigmoid')(x)
model = Model(inputs=vgg.input, outputs=prediction)
fc1 = model.layers[-1]
fc2 = model.layers[-2]
fc3 = model.layers[-3]
fc4 = model.layers[-4]
fc5 = model.layers[-5]
fc6 = model.layers[-6]
fc7 = model.layers[-7]
fc8 = model.layers[-8]
fc9 = model.layers[-9]
fc10 = model.layers[-10]
fc11 = model.layers[-11]
fc12 = model.layers[-12]
fc13 = model.layers[-13]
fc14 = model.layers[-14]
fc15 = model.layers[-15]
fc16 = model.layers[-16]
fc17 = model.layers[-17]
fc18 = model.layers[-18]
fc19 = model.layers[-19]
fc20 = model.layers[-20]
fc21 = model.layers[-21]
drop=Dropout(0.08)
layer1=Input([224,224,3])
layer=fc20(layer1)
layer=fc19(layer)
drop=Dropout(0.08)(layer)
layer=fc18(drop)
layer=fc17(layer)
drop=Dropout(0.08)(layer)
layer=fc16(layer)
layer=fc15(layer)
layer=fc14(layer)
layer=fc13(layer)
layer=fc12(layer)
drop=Dropout(0.08)(layer)
layer=fc11(layer)
layer=fc10(layer)
layer=fc9(layer)
layer=fc8(layer)
drop=Dropout(0.08)(layer)
layer=fc7(layer)
layer=fc6(layer)
layer=fc5(layer)
layer=fc4(layer)
drop=Dropout(0.08)(layer)
layer=fc3(layer)
layer=fc2(layer)
layer=fc1(layer)
model=Model(inputs=layer1, outputs=layer)
model.compile(
  loss='binary_crossentropy',
  optimizer='adam',
  metrics=['accuracy']
)


r = model.fit(
  training_set,
  validation_data=validation_set,
  epochs=5,
  steps_per_epoch=len(training_set),
  validation_steps=len(validation_set)
)
model.save(r'/home/ubuntu/tmface1/model.h5')
print("model saved")
img_size=224
datadir=r"/home/ubuntu/tmface1/test"
training_data=[]
categories=["male","female"]
for category in categories:
    path=os.path.join(datadir,category)
    class_num=categories.index(category)
    for img in os.listdir(path):
        try:
            image = cv2.imread(os.path.join(path,img))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (img_size, img_size))
            training_data.append([image, class_num])
        except Exception as e:
            pass
random.shuffle(training_data)
x_test=[]
y_test=[]
for image,gender in training_data:
    x_test.append(image)
    y_test.append(gender)
X_train=[]
for i in range(len(x_test)):
    img_array=asarray(x_test[i])
    X_train.append(img_array)
x_test=X_train
x_test=np.array(x_test)
x_test=x_test.reshape(-1,img_size,img_size,3)
y_pred=model.predict(x_test)



y_pred=np.round(y_pred)
from sklearn.metrics import confusion_matrix
cm=confusion_matrix(y_pred,y_test)
model_accuracy=(cm[0][0]+cm[1][1]) / ( cm[1][0] + cm[0][1]+cm[0][0]+cm[1][1])
print(model_accuracy)




